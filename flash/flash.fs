
module Types =

  // use these types & public server & client functions to document endpoints

  type UserId = int
  type UserIds = UserId list

  // switch to degrees
  type Location = {x: decimal; y: decimal}

  type Time = int

  // include interests, timestamp
  type Request = { userId: UserId; location: Location }
  type Requests = Request list

  type Interest = string
  type Interests = Interest list

  // include interests, timestamp(?)
  type Suggestion = {location: Location; userId: UserId} 
  type Suggestions = Suggestion list

module Server =

  open Types

  type State = Map<UserId, Request>

  let init : State = Map.empty

  // save for later realtime processing, use either:
  // a) third party service - Google's Big Query?
  // b) Graphite or Elasticsearch?
  let receive (state : State) (request : Request) : State = state.Add(request.userId, request)

  // use cluster algorithm
  // send multiple suggestions, to many users
  let send (_state: State) : Suggestion = {location = {x = 0M; y = 0M}; userId = 1 }
      
module Client =

  open Types
  // include clientside suggestion deletion(?)

  // include interests, time stamp
  type State = { userId: UserId; location: Location; suggestion: Suggestion option}
  
  let init userId location : State = { userId = userId; location = location; suggestion = None} 

  let send state : Request = { userId = state.userId; location = state.location}

  let receive state suggestion  = {state with suggestion = Some( suggestion ) }

module Cluster =

  // http://www.oreilly.com/programming/free/analyzing-visualizing-data-f-sharp.csp

  // calculate based on longitude & latitude degrees etc.
  // include interests & time(?) as part of cluster analysis
  let distance ( a : Types.Location ) ( b : Types.Location ) : float = 
    [ (a.y - b.y); (a.x - b.x) ] 
    |> List.sumBy (fun x -> pown x 2) 
    |> float 
    |> sqrt

  let aggregatePoints points : float * float = (List.averageBy fst points, List.averageBy snd points)

  // calculate dynamically
  let clusterCount = 3

  let centroids = 
    let random = System.Random()
    [1..clusterCount] 
    |> List.map (fun _x -> random.Next())

  let closest centroids input = 
    centroids 
    |> List.mapi (fun i v -> i, v) 
    |> List.minBy (fun(_, cent) -> distance cent input) 
    |> fst

module Data =

  open FSharp.Data

  // include LA FourSquare check-in venues, taxi & crowd flow data
  // from: https://www.microsoft.com/en-us/research/project/urban-computing/

  //use relative path
  let checkins = @"C:\Users\John\Dropbox\programming\fs\flash\flash\data\NYC-Venues.txt"

  //add / proxy time stamp to data
  type CheckInFormat = CsvProvider< @"C:\Users\John\Dropbox\programming\fs\flash\flash\data\NYC-Venues.txt" 
     , "\t"
     , HasHeaders = false
     , IgnoreErrors = true
     , Schema = "Venue id, venue name, latitude, longitude, address, city, state, checkin #, checked user#, current user#, todo#" >

module Flash =

  open Types

  type ClientStates = Map<UserId, Client.State>
  type GlobalState = {clients: ClientStates; serverState: Server.State}
  let initState = {clients = Map.empty ; serverState = Server.init}

  // break into async actor model (?) come closer to modelling real clients & server etc.
  let simulation : GlobalState = 
    Data.CheckInFormat.Load(Data.checkins).Rows
    // |> Seq.take 100
    |> Seq.fold (fun a c -> 
        // create / update client  
        Client.init c.``Checked user#`` { x = c.Longitude; y = c.Latitude }
        |> fun client ->        
          // add / update client to ClientStates Map
          {a with clients = a.clients.Add( c.``Checked user#``, client )
                  // send request to server
                  serverState = client 
                  |> Client.send 
                  |> Server.receive a.serverState
          }
        |> fun gs ->
          {
            // send suggestion to client(s)
            gs with clients = gs.serverState 
                                |> Server.send 
                                |> fun suggestion -> 
                                          // if cannot find client anymore, then do no update
                                          match (gs.clients.TryFind suggestion.userId) with
                                          | Some c -> (suggestion.userId, Client.receive c suggestion) |> gs.clients.Add
                                          | None -> gs.clients
          }
        
        )
      initState

  [<EntryPoint>]
  let main _argv =
      // add charting of results
      printfn "%A" simulation
      0

