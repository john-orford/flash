AGPLv3 John Orford 2018


# README

This is the beginning of a simulation program, which uses realworld sample geolocation data.

The data currently includes Four Square check-in data.

The simulation reads data from files and then *aims* to match people together.


## Build

Due to this bug

https://github.com/Microsoft/visualfsharp/issues/3303

`flash\fsc.props` needs to reference local .Net Framework compiler

The data uses an absolute path, which needs to be updated (in future change to relative).


## Todo List

Many things, listed in comments in the code here: `flash/flash.fs`